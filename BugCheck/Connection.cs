﻿// ***********************************************************************
// Assembly         : BugCheck
// Author           : Tom
// Created          : 01-05-2018
//
// Last Modified By : Tom
// Last Modified On : 01-06-2018
// ***********************************************************************
// <summary>Class to store SQL connection data and manage the connection state</summary>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugCheck
{
    /// <summary>
    /// Class Connection.
    /// </summary>
    class Connection
    {
        static String path = Directory.GetCurrentDirectory() + "\\";

        /// <summary>
        /// The connection
        /// </summary>
        SqlConnection con = new SqlConnection(@"Data Source = (LocalDB)\MSSQLLocalDB;AttachDbFilename="+path+"BugCheckDB.mdf;Integrated Security = True; Connect Timeout = 30");

        /// <summary>
        /// Strings the connection.
        /// </summary>
        /// <returns>SqlConnection.</returns>
        public SqlConnection String() => con;

        /// <summary>
        /// Opens the connection.
        /// </summary>
        public void Open()
        {
            if (con.State == ConnectionState.Open)
            {
                con.Close();
            }
            else
            {
                con.Open();
            }
        }

        /// <summary>
        /// Closes the connection.
        /// </summary>
        public void Close() => con.Close();
    }
}
