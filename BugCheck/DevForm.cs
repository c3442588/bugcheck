﻿// ***********************************************************************
// Assembly         : BugCheck
// Author           : Tom
// Created          : 01-03-2018
//
// Last Modified By : Tom
// Last Modified On : 01-06-2018
// ***********************************************************************
// <summary>A form which handles the developer side of the application</summary>
// ***********************************************************************
using System;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;
using ScintillaNET;
using ClosedXML.Excel;

namespace BugCheck
{
    /// <summary>
    /// Class DevForm.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class DevForm : Form
    {
        /// <summary>
        /// Stores the username of the developer
        /// </summary>
        String user;
        /// <summary>
        /// Stores an instance of the login form
        /// </summary>
        LoginForm login;
        /// <summary>
        /// The SQL connection info
        /// </summary>
        Connection connection;
        /// <summary>
        /// Allows for simpler manipulation of an sql command
        /// </summary>
        SqlCommand cmd;
        /// <summary>
        /// Allows for simpler manipulation of an sql data adapter
        /// </summary>
        SqlDataAdapter adapt;
        /// <summary>
        /// Holds the primary keys for the project and bug table
        /// </summary>
        int ID,bugID = 0;

        /// <summary>
        /// Initializes a new instance of the <see cref="DevForm"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="login">The login.</param>
        public DevForm(String user, LoginForm login)
        {
            InitializeComponent();
            this.user = user;
            this.login = login;
            this.CenterToScreen();
            connection = new Connection();
            scintillaLoad();            
        }

        /// <summary>
        /// Handles the Load event of the DevForm control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void DevForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bugCheckDBDataSet1.BugAudit' table. You can move, or remove it, as needed.
            this.bugAuditTableAdapter.Fill(this.bugCheckDBDataSet1.BugAudit);
            // TODO: This line of code loads data into the 'bugCheckDBDataSet.Bug' table. You can move, or remove it, as needed.
            this.bugTableAdapter.Fill(this.bugCheckDBDataSet.Bug);
            // TODO: This line of code loads data into the 'bugCheckDBDataSet.Project' table. You can move, or remove it, as needed.
            this.projectTableAdapter.Fill(this.bugCheckDBDataSet.Project);
        }

        /// <summary>
        /// Updates the project datagridview with the latest project table information
        /// </summary>
        private void DisplayData()
        {
            connection.Open();
            DataTable dt = new DataTable();
            adapt = new SqlDataAdapter("select * from Project", connection.String());
            adapt.Fill(dt);
            dataGridView1.DataSource = dt;
            connection.Close();
        }

        /// <summary>
        /// Updates the bug datagridview with the latest bug table information relating to it parent project
        /// </summary>
        private void bugData()
        {
            connection.Open();
            DataTable dt = new DataTable();
            adapt = new SqlDataAdapter("select * from Bug where Project_ID=@id", connection.String());
            adapt.SelectCommand.Parameters.AddWithValue("@id", ID);
            adapt.Fill(dt);
            dataGridView2.DataSource = dt;
            connection.Close();
        }

        /// <summary>
        /// Updates the datagridview with the latest bugaudit table information relating to its parent bug
        /// </summary>
        private void auditData()
        {
            connection.Open();
            DataTable dt = new DataTable();
            adapt = new SqlDataAdapter("select * from BugAudit where Bug_ID=@id", connection.String());
            adapt.SelectCommand.Parameters.AddWithValue("@id", bugID);
            adapt.Fill(dt);
            dataGridView3.DataSource = dt;
            connection.Close();
        }

        /// <summary>
        /// Clears the data.
        /// </summary>
        private void ClearData()
        {
            textBox1.Text = "";
            ID = 0;
            checkBox_confirm.Checked = false;            
        }

        /// <summary>
        /// Opens the login window and closes this instance of the developer form
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            login.Show();
            this.Close();
        }

        /// <summary>
        /// Closes the hidden login form once the developer window is closed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="FormClosedEventArgs"/> instance containing the event data.</param>
        private void DevForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            login.Close();         
        }

        /// <summary>
        /// Inserts the new project row into the project table after the create button is clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button_create_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                cmd = new SqlCommand("insert into Project(Name) values(@name)", connection.String());
                connection.Open();
                cmd.Parameters.AddWithValue("@name", textBox1.Text);
                cmd.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Project Created Successfully");
                DisplayData();
                ClearData();
            }
            else
            {
                MessageBox.Show("Please Provide a Project Name");
            }
        }

        /// <summary>
        /// Updates the selected project row in the project table after the update button is clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button_update_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                cmd = new SqlCommand("update Project set Name=@name where ID=@id", connection.String());
                connection.Open();
                cmd.Parameters.AddWithValue("@id", ID);
                cmd.Parameters.AddWithValue("@name", textBox1.Text);                
                cmd.ExecuteNonQuery();
                MessageBox.Show("Project Name Updated Successfully");
                connection.Close();
                DisplayData();
                ClearData();
            }
            else
            {
                MessageBox.Show("Please Select Record to Update");
            }
        }

        /// <summary>
        /// Deletes the selected row in the project table after the delete button is clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button_delete_Click(object sender, EventArgs e)
        {
            if (ID != 0)
            {
                cmd = new SqlCommand("delete Project where ID=@id", connection.String());
                connection.Open();
                cmd.Parameters.AddWithValue("@id", ID);
                cmd.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Project Deleted Successfully!");
                DisplayData();
                bugData();
                auditData();
                ClearData();
            }
            else
            {
                MessageBox.Show("Please Select Project to Delete");
            }
        }

        /// <summary>
        /// Commits any code changes made in the scintilla textbox after the commit button is clicked with confirmation.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button_commit_Click(object sender, EventArgs e)
        {
            if (checkBox_confirm.Checked)
            {
                cmd = new SqlCommand("update Project set Code=@code where ID=@id", connection.String());
                connection.Open();
                cmd.Parameters.AddWithValue("@code", scintilla1.Text);
                cmd.Parameters.AddWithValue("@id", ID);
                cmd.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Code Updated Successfully");
                DisplayData();
                ClearData();
            }
            else
            {
                MessageBox.Show("Please Confirm code changes before continuing");
            }
        }

        /// <summary>
        /// Set the lexer style, colours and keywords within the scintilla textbox to automatically colour and format 
        /// the text based on its syntax currently only for c#       
        /// </summary>
        private void scintillaLoad()
        {
            scintilla1.Margins[0].Width = 32;            
            scintilla1.StyleResetDefault();
            scintilla1.Styles[Style.Default].Font = "Consolas";
            scintilla1.Styles[Style.Default].Size = 10;
            scintilla1.StyleClearAll();

            
            scintilla1.Styles[Style.Cpp.Default].ForeColor = Color.Silver;
            scintilla1.Styles[Style.Cpp.Comment].ForeColor = Color.FromArgb(0, 128, 0); // Green
            scintilla1.Styles[Style.Cpp.CommentLine].ForeColor = Color.FromArgb(0, 128, 0); // Green
            scintilla1.Styles[Style.Cpp.CommentLineDoc].ForeColor = Color.FromArgb(128, 128, 128); // Gray
            scintilla1.Styles[Style.Cpp.Number].ForeColor = Color.Olive;
            scintilla1.Styles[Style.Cpp.Word].ForeColor = Color.Blue;
            scintilla1.Styles[Style.Cpp.Word2].ForeColor = Color.Blue;
            scintilla1.Styles[Style.Cpp.String].ForeColor = Color.FromArgb(163, 21, 21); // Red
            scintilla1.Styles[Style.Cpp.Character].ForeColor = Color.FromArgb(163, 21, 21); // Red
            scintilla1.Styles[Style.Cpp.Verbatim].ForeColor = Color.FromArgb(163, 21, 21); // Red
            scintilla1.Styles[Style.Cpp.StringEol].BackColor = Color.Pink;
            scintilla1.Styles[Style.Cpp.Operator].ForeColor = Color.Purple;
            scintilla1.Styles[Style.Cpp.Preprocessor].ForeColor = Color.Maroon;
            scintilla1.Lexer = Lexer.Cpp;
            
            scintilla1.SetKeywords(0, "abstract as base break case catch checked continue default delegate do else event explicit extern false finally fixed for foreach goto if implicit in interface internal is lock namespace new null object operator out override params private protected public readonly ref return sealed sizeof stackalloc switch this throw true try typeof unchecked unsafe using virtual while");
        }

        /// <summary>
        /// Populates the bug datagridview when a row is selected in the project datagridview and fills the scintilla textbox 
        /// with the related code
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs"/> instance containing the event data.</param>
        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            ID = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            textBox1.Text = dataGridView1.Rows[e.RowIndex].Cells[1].Value.ToString();
            scintilla1.Text = dataGridView1.Rows[e.RowIndex].Cells[2].Value.ToString();
            bugData();
            if (dataGridView2.Rows.Count == 0)
            {
                bugID = 0;
                auditData();
            }
        }

        /// <summary>
        /// Populates the bugAudit datagridview when a row is selected in the bug datagridview
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs"/> instance containing the event data.</param>
        private void dataGridView2_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            bugID = Convert.ToInt32(dataGridView2.Rows[e.RowIndex].Cells[0].Value.ToString());
            auditData();
        }

        /// <summary>
        /// Displays the audit description in a messagebox when a row is double clicked on the the bugaudit datagridview
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs"/> instance containing the event data.</param>
        private void dataGridView3_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            MessageBox.Show(dataGridView3.Rows[e.RowIndex].Cells[3].Value.ToString(), "Fix Description:");
        }

        /// <summary>
        /// Exports the selected bug report datatable as an excel document
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void bugReportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Title = "Select Excel Save location";
                saveFileDialog1.Filter = "Excel |*.xlsx";
                saveFileDialog1.ShowDialog();
                connection.Open();
                XLWorkbook wb = new XLWorkbook();
                DataTable dt = new DataTable();
                adapt = new SqlDataAdapter("select * from Bug where Project_ID=@id", connection.String());
                adapt.SelectCommand.Parameters.AddWithValue("@id", ID);
                adapt.Fill(dt);
                wb.Worksheets.Add(dt, "Bug Report");
                wb.SaveAs(saveFileDialog1.FileName);
                connection.Close();
            }catch(Exception ex)
            {

            }
            
        }

        /// <summary>
        /// Exports the selected audit trail datatable as an excel document
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void auditTrailToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.Title = "Select Excel Save location";
                saveFileDialog1.Filter = "Excel |*.xlsx";
                saveFileDialog1.ShowDialog();
                connection.Open();
                XLWorkbook wb = new XLWorkbook();
                DataTable dt = new DataTable();
                adapt = new SqlDataAdapter("select * from BugAudit where Bug_ID=@id", connection.String());
                adapt.SelectCommand.Parameters.AddWithValue("@id", bugID);
                adapt.Fill(dt);
                wb.Worksheets.Add(dt, "Audit Report");
                wb.SaveAs(saveFileDialog1.FileName);
                connection.Close();
            }
            catch (Exception ex)
            {

            }

        }





        /// <summary>
        /// Submits the audit to the associated bug oce the submit button is clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button_audit_submit_Click(object sender, EventArgs e)
        {
            try
            {
                if (text_audit_desc.Text != "")
                {
                    cmd = new SqlCommand("insert into BugAudit(Developer, Line_No, Fix_Desc, Bug_ID) values(@Developer, @Line_No, @desc, @Bug_ID)", connection.String());
                    connection.Open();
                    cmd.Parameters.AddWithValue("@Developer", user);
                    cmd.Parameters.AddWithValue("@Line_No", numericUpDown_line.Value);
                    cmd.Parameters.AddWithValue("@desc", text_audit_desc.Text);
                    cmd.Parameters.AddWithValue("@Bug_ID", bugID);
                    cmd.ExecuteNonQuery();
                    if (checkBox1.Checked == true)
                    {
                        cmd = new SqlCommand("update Bug set Fixed=1 where Project_ID=@id", connection.String());
                        cmd.Parameters.AddWithValue("@ID", ID);
                        cmd.ExecuteNonQuery();
                        bugData();
                    }
                    connection.Close();
                    MessageBox.Show("Audit Recorded Successfully");
                    auditData();
                    checkBox1.Checked = false;
                    numericUpDown_line.Value = 0;
                    text_audit_desc.Text = "";
                }
                else
                {
                    MessageBox.Show("Please Provide a description of the bug fix");
                }
            }
            catch (SqlException ex)
            {
                MessageBox.Show(ex.ToString());               
            }
            connection.Close();
        }
    }
}
