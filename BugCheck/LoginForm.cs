﻿// ***********************************************************************
// Assembly         : BugCheck
// Author           : Tom
// Created          : 01-03-2018
//
// Last Modified By : Tom
// Last Modified On : 01-05-2018
// ***********************************************************************
// <summary>A form which handles the user login and registration process</summary>
// ***********************************************************************
using System;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace BugCheck
{
    /// <summary>
    /// Class LoginForm.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class LoginForm : Form
    {
        /// <summary>
        /// The SQL connection information
        /// </summary>
        Connection connection;

        /// <summary>
        /// Constructor for the form
        /// </summary>
        public LoginForm()
        {
            InitializeComponent();
            this.CenterToScreen();
            connection = new Connection();
        }

        /// <summary>
        /// Handles the Load event of the Form1 control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Handles the Click event of the button1 control which logs the user into the program.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                String Table = null;
                if (devRadioButton.Checked)
                {
                    Table = "Developer";
                }
                else if (testerRadioButton.Checked)
                {
                    Table = "Tester";
                }
                connection.Open();
                SqlCommand cmd = new SqlCommand(@"SELECT Count(*) FROM "+Table+" WHERE Username=@uname and Password=@pass", connection.String());
                cmd.Parameters.AddWithValue("@uname", txtUsername.Text);
                cmd.Parameters.AddWithValue("@pass", txtPassword.Text);
                
                int result = (int)cmd.ExecuteScalar();
                if (result > 0)
                {
                    MessageBox.Show("Login Success");
                    
                    this.Hide();
                    if (devRadioButton.Checked)
                    {
                        DevForm devForm = new DevForm(txtUsername.Text, this);
                        devForm.Show();
                    }
                    else if (testerRadioButton.Checked)
                    {
                        TesterForm testerForm = new TesterForm(txtUsername.Text,this);
                        testerForm.Show();
                    }
                    
                    txtUsername.Text = "";
                    txtPassword.Text = "";
                }                    
                else
                {
                    MessageBox.Show("Incorrect login");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unexpected error:" + ex.Message);
                
            }
            connection.Close();
        }



        /// <summary>
        /// Handles the Click event of the button2 control which registers a user with the information agthered form the assoicated text boxes.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                String Table = null;
                if (devRadioButton.Checked)
                {
                    Table = "Developer";
                }
                else if (testerRadioButton.Checked)
                {
                    Table = "Tester";
                }
                connection.Open();
                SqlCommand cmd = new SqlCommand(@"INSERT INTO " + Table + "(Username, Password) VALUES(@uname, @pass)", connection.String());
                cmd.Parameters.AddWithValue("@uname", txtUsername.Text);
                cmd.Parameters.AddWithValue("@pass", txtPassword.Text);
                cmd.ExecuteNonQuery();   
                connection.Close();                
                MessageBox.Show("Account Created");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unexpected error:" + ex.Message);
                connection.Close();
            }

        }
    }
}
