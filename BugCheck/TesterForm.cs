﻿// ***********************************************************************
// Assembly         : BugCheck
// Author           : Tom
// Created          : 01-03-2018
//
// Last Modified By : Tom
// Last Modified On : 01-06-2018
// ***********************************************************************
// <summary>A form which handles the tester side of the application</summary>
// ***********************************************************************
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;


namespace BugCheck
{
    /// <summary>
    /// Class TesterForm.
    /// </summary>
    /// <seealso cref="System.Windows.Forms.Form" />
    public partial class TesterForm : Form
    {
        /// <summary>
        /// Stores an instance of the login form
        /// </summary>
        LoginForm login;
        /// <summary>
        /// Stores the username of the tester
        /// </summary>
        String user;
        /// <summary>
        /// The SQL connection info
        /// </summary>
        Connection connection;
        /// <summary>
        /// Holds the primary key for the project table
        /// </summary>
        int ID;
        /// <summary>
        /// Allows for simpler manipulation of an sql data adapter
        /// </summary>
        SqlDataAdapter adapt;
        /// <summary>
        /// Allows for simpler manipulation of an sql command
        /// </summary>
        SqlCommand cmd;

        /// <summary>
        /// Initializes a new instance of the <see cref="TesterForm"/> class.
        /// </summary>
        /// <param name="user">The user.</param>
        /// <param name="login">The login.</param>
        public TesterForm(String user, LoginForm login)
        {
            InitializeComponent();
            this.user = user;
            this.login = login;
            this.CenterToScreen();
            connection = new Connection();
            ClearData();
            bugData();
            DisplayData();
        }

        /// <summary>
        /// Handles the Load event of the TesterForm control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void TesterForm_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bugCheckDBDataSet1.Bug' table. You can move, or remove it, as needed.
            this.bugTableAdapter.Fill(this.bugCheckDBDataSet1.Bug);
            // TODO: This line of code loads data into the 'bugCheckDBDataSet.Project' table. You can move, or remove it, as needed.
            this.projectTableAdapter.Fill(this.bugCheckDBDataSet.Project);
        }

        /// <summary>
        /// Updates the project datagridview with the latest project table information
        /// </summary>
        private void DisplayData()
        {
            connection.Open();
            DataTable dt = new DataTable();
            adapt = new SqlDataAdapter("select * from Project", connection.String());
            adapt.Fill(dt);
            dataGridView1.DataSource = dt;
            connection.Close();
        }

        /// <summary>
        /// Updates the bug datagridview with the latest bug table information relating to it parent project
        /// </summary>
        private void bugData()
        {
            connection.Open();
            DataTable dt = new DataTable();
            adapt = new SqlDataAdapter("select * from Bug where Project_ID=@id", connection.String());
            adapt.SelectCommand.Parameters.AddWithValue("@id", ID);
            adapt.Fill(dt);
            dataGridView2.DataSource = dt;
            connection.Close();
        }

        /// <summary>
        /// Closes the hidden login form once the tester window is closed
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="FormClosedEventArgs"/> instance containing the event data.</param>
        private void TesterForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            login.Close();       
        }

        /// <summary>
        /// Opens the login window and closes this instance of the developer form
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void logoutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            login.Show();
            this.Close();
        }

        /// <summary>
        /// Populates the bug datagridview when a row is selected in the project datagridview
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="DataGridViewCellEventArgs"/> instance containing the event data.</param>
        private void dataGridView1_RowEnter(object sender, DataGridViewCellEventArgs e)
        {
            ID = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString());
            bugData();

        }

        /// <summary>
        /// Clears the data.
        /// </summary>
        private void ClearData()
        {
            text_symptom.Text = "";
            text_cause.Text = "";
            ID = 0;            
        }

        /// <summary>
        /// Inserts the bug information into the bug table once the submit button is clicked
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void button1_Click(object sender, EventArgs e)
        {
            if (text_symptom.Text != "" || text_cause.Text != "")
            {
                cmd = new SqlCommand("insert into Bug(Symptom, Cause, Tester, Project_ID) values(@Symptom, @Cause, @Tester, @Project_ID)", connection.String());
                connection.Open();
                cmd.Parameters.AddWithValue("@Symptom", text_symptom.Text);
                cmd.Parameters.AddWithValue("@Cause", text_cause.Text);
                cmd.Parameters.AddWithValue("@Tester", user);
                cmd.Parameters.AddWithValue("@Project_ID", ID);
                cmd.ExecuteNonQuery();
                connection.Close();
                MessageBox.Show("Bug Report Created Successfully!");
                bugData();
                ClearData();
            }
            else
            {
                MessageBox.Show("Please provide details of the bug.");
            }
        }
    }
}
